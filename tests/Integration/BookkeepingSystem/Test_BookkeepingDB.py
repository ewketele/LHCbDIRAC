###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
"""
Tests basic methods from the BookkeepingDB
It requires an Oracle database
"""

import sys

import pytest

from DIRAC.Core.Utilities.ReturnValues import S_OK, SErrorException, returnValueOrRaise
from .Utilities import wipeOutDB


@pytest.fixture
def bkkDB():
    from DIRAC.Core.Base.Script import parseCommandLine

    argv, sys.argv = sys.argv, sys.argv[:1]
    parseCommandLine()
    sys.argv = argv + ["-ddd"]
    from LHCbDIRAC.BookkeepingSystem.DB.OracleBookkeepingDB import OracleBookkeepingDB

    yield OracleBookkeepingDB()


def test_dataTakingConditions(bkkDB):
    assert returnValueOrRaise(bkkDB._getDataTakingConditionId("NonExistingDescription")) == -1

    # Insert a condition with only the description, and check the id
    dtcId = returnValueOrRaise(bkkDB.insertDataTakingCondDesc("JustADescription"))
    try:
        assert returnValueOrRaise(bkkDB._getDataTakingConditionId("JustADescription")) == dtcId
    finally:
        returnValueOrRaise(bkkDB.deleteDataTakingCondition(dtcId))

    # Make sure the deletion has worked
    assert returnValueOrRaise(bkkDB._getDataTakingConditionId("JustADescription")) == -1

    dataTakingDict = {
        "Description": "Description",
        "BeamCond": "BeamCond",
        "BeamEnergy": "BeamEnergy",
        "MagneticField": "MagneticField",
        "VELO": "VELO",
        "IT": "IT",
        "TT": "TT",
        "OT": "OT",
        "RICH1": "RICH1",
        "RICH2": "RICH2",
        "SPD_PRS": "SPD_PRS",
        "ECAL": "ECAL",
        "HCAL": "HCAL",
        "MUON": "MUON",
        "L0": "L0",
        "HLT": "HLT",
        "VeloPosition": "VeloPosition",
    }

    # Insert a condition with the full detector details (this is what we want to suppress)
    dtcId = returnValueOrRaise(bkkDB.insertDataTakingCond(dataTakingDict))
    # Get it back
    try:
        assert returnValueOrRaise(bkkDB._getDataTakingConditionId("Description")) == dtcId
    finally:
        returnValueOrRaise(bkkDB.deleteDataTakingCondition(dtcId))


def test_SMOG2State(bkkDB):
    # unsetSMOG2State() is not foreseen
    wipeOutDB(bkkDB)
    # Check the list of possible states
    states = returnValueOrRaise(bkkDB.getAvailableSMOG2States())
    assert len(states) == 20 and type(states[0]) is str
    # Insert 2 states
    assert returnValueOrRaise(bkkDB.setSMOG2State("Oxygen", False, [1000, 1001])) is None
    # Updating should also be fine, as long as the state is the same
    assert returnValueOrRaise(bkkDB.setSMOG2State("Oxygen", False, [1000, 1001])) is None
    # But updating with different state should fail...
    with pytest.raises(SErrorException) as excinfo:
        returnValueOrRaise(bkkDB.setSMOG2State("NoGas", False, [1000]))
    assert "different SMOG2 state" in str(excinfo.value)
    # ... till explicitly asked
    returnValueOrRaise(bkkDB.setSMOG2State("NoGas", True, [1000]))
    # Undefined SMOG2 state
    with pytest.raises(SErrorException) as excinfo:
        returnValueOrRaise(bkkDB.setSMOG2State("UnknownState", False, [5000]))
    assert "is unknown" in str(excinfo.value)
    # Check that data are fine
    assert set(returnValueOrRaise(bkkDB.getRunsForSMOG2("Oxygen"))) == {1001}


def test_ExtendedDQOK(bkkDB):
    wipeOutDB(bkkDB)
    # unset not existing run, should be fine (nothing to do)
    assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, False, [])) is None
    # set for new run
    assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, False, ["SMOG2", "AA"])) is None
    # extend for existing run, should fail (not allowed)
    with pytest.raises(SErrorException) as excinfo:
        assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, False, ["SMOG2", "AA", "BB"]))
    # extend for existing run, should be fine (allowed)
    assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, True, ["SMOG2", "AA", "BB"])) is None
    # extend and remove for existing run, should fail (not allowed)
    with pytest.raises(SErrorException) as excinfo:
        assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, False, ["SMOG2", "BB", "CC"]))
    # extend and remove for existing run, should be fine (allowed)
    assert returnValueOrRaise(bkkDB.setExtendedDQOK(5000, True, ["SMOG2", "BB", "CC"])) is None
    # set for yet another runs
    assert returnValueOrRaise(bkkDB.setExtendedDQOK(5001, False, ["SMOG2", "BB", "DD"])) is None
    assert (
        returnValueOrRaise(
            bkkDB.setExtendedDQOK(
                5002,
                False,
                [
                    "SMOG2",
                    "AA",
                ],
            )
        )
        is None
    )
    # check that data are as expected
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK(["SMOG2"]))) == {5000, 5001, 5002}
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK(["AA"]))) == {5002}
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK(["SMOG2", "BB"]))) == {5000, 5001}
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK(["SMOG2", "BB", "DD"]))) == {5001}
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK(["EE"]))) == set()
    assert set(returnValueOrRaise(bkkDB.getRunsWithExtendedDQOK([]))) == set()
