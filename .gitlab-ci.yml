###############################################################################
# (c) Copyright 2019 CERN for the benefit of the LHCb Collaboration           #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "LICENSE".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# This gitlab ci tests, deploys and build the docker images
# It performs some operations with an account.
# Its credentials are stored as secret variable KRB_PASSWORD and KRB_USERNAME
# Image used for most of the jobs, unless specified
image: registry.cern.ch/docker.io/library/almalinux:9

stages:
  # sweep immediately after merger
  - sweep
   # Run all the tests
  - test
  #  build images
  - images
  # If there's a new tag, build the docker images, and also deploy on CVMFS
  - upload_pypi
  - deploy_release
  - update
  # Left over tasks like tagging the devel container and setting the prod symlink
  - finish_up
  - tag


### test stage ###

pre-commit:
  image: registry.cern.ch/docker.io/library/python:3.11
  stage: test
  before_script:
    - pip install pre-commit
  script:
    - pre-commit run --all-files
  variables:
    PRE_COMMIT_HOME: ${CI_PROJECT_DIR}/.cache/pre-commit
  cache:
    paths:
      - ${PRE_COMMIT_HOME}

# Common configuration between pytest, pylint, pycodestyle
.test_setup:
  stage: test
  image: registry.cern.ch/docker.io/condaforge/miniforge3:latest
  before_script:
    - eval "$(python -m conda shell.bash hook)"
    - conda install --yes diraccfg git jq curl
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git && git fetch upstream
    # Find the latest compatible DIRAC release
    - diracBranch=$(grep 'DIRAC ~=' setup.cfg | sed -E 's@.+~= *([0-9])+\.([0-9]+)\.[0-9]+((a|b|rc)[0-9]+)?@rel-v\1r\2@g')
    - echo "Found DIRAC branch $diracBranch"
    # Query GitHub to find if the rel-vXrY branch exists, fall back to integration if it does not
    - |
      if ! curl --fail --no-progress-meter --head -L "https://codeload.github.com/DIRACGrid/DIRAC/zip/refs/heads/${diracBranch}" > /dev/null; then
        diracBranch="integration"
      fi
    - echo "diracBranch=${diracBranch}"
    # Checkout the latest compatible DIRAC release
    - git clone https://github.com/DIRACGrid/DIRAC.git -b "${diracBranch}" ../DIRAC
    - cd ../DIRAC
    # Now take the code from the release
    - diracTag=$(git describe --tags --long --match '*[0-9]*' --exclude 'v[0-9]r*' --exclude 'v[0-9][0-9]r*' | cut -d '-' -f 1)
    - echo "diracTag=${diracTag}"
    - git checkout "${diracTag}"
    # Create the environment and install DIRAC+LHCbDIRAC
    # We take the environment from the branch and not the release
    # to not depend on a release to change the dependencies.
    - git checkout ${diracBranch} -- environment.yml
    - conda env create --file environment.yml --name test-env
    - conda activate test-env
    - pip install .[server]
    - cd ../LHCbDIRAC
    - pip install .[server]
    - pip install pycodestyle

run_pylint:
  extends: .test_setup
  script:
    - pylint -j 0 -E src/ tests/

run_pytest:
  extends: .test_setup
  script:
    - DIRAC_DEPRECATED_FAIL=True pytest -k src

bookkeeping_tests:
  extends: .test_setup
  rules:
    # Using rules causes "Pipelines for Merge Requests" to be triggered, so disable them explicitly
    # https://docs.gitlab.com/ee/ci/merge_request_pipelines/
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD == null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: true
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD != null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: false
  services:
    - name: gitlab-registry.cern.ch/lhcb-dirac/bookkeeping-ci-container/oracle-database-snapshots:19.3.0-ee
      alias: oracledb
      variables:
        ORACLE_SID: bkdbsid
        ORACLE_PDB: bkdbpdb
        ORACLE_PWD: bkdbpass
        ORACLE_CHARACTERSET: AL32UTF8
  script:
    - |
        apt update && apt install -y unzip libaio1 && \
        export INSTANTCLIENT_DIR=$(mktemp -d) && \
        cd $INSTANTCLIENT_DIR && \
        curl -LO https://download.oracle.com/otn_software/linux/instantclient/219000/instantclient-basic-linux.x64-21.9.0.0.0dbru.zip && \
        unzip instantclient-basic-linux.x64-21.9.0.0.0dbru.zip && \
        curl -LO https://download.oracle.com/otn_software/linux/instantclient/219000/instantclient-sqlplus-linux.x64-21.9.0.0.0dbru.zip && \
        unzip instantclient-sqlplus-linux.x64-21.9.0.0.0dbru.zip && \
        cd - && \
        echo "DISABLE_OOB=ON" >> ~/.sqlnet.ora
    - |
        cat tests/Jenkins/bookkeeping_install/create_schema_and_procedures.sql | \
        LD_LIBRARY_PATH=$INSTANTCLIENT_DIR/instantclient_21_9 ORACLE_HOME=$INSTANTCLIENT_DIR $INSTANTCLIENT_DIR/instantclient_21_9/sqlplus system/bkdbpass@//oracledb:1521/BKDBPDB
    - DIRAC_DEPRECATED_FAIL=True pytest -k 'Test_BookkeepingDB' --oracle-url oracle://system:bkdbpass@oracledb:1521/bkdbpdb

pycodestyle:
  extends: .test_setup
  script:
    - pycodestyle

# Check that the documentation does not generate any error/warning
check_docs:
  extends: .test_setup
  script:
    - pip install ../DIRAC/docs
    - cd docs
    # We need to export the DIRAC env variable so that the makefile knows where to find the documentation tools
    - export DIRACDOCTOOLS=$(dirname $(python -c "import diracdoctools; print(diracdoctools.__file__)"))
    - SPHINXOPTS=-wsphinxWarnings make htmlall || { echo "Failed to build documentation, check for errors above" ; exit 1; }
    # Check that sphinxWarnings is not empty
    - >
      if [ -s sphinxWarnings ]; then
        echo "********************************************************************************"
        echo "Warnings When Creating Doc:"
        echo "********************************************************************************"
        cat sphinxWarnings
        echo "********************************************************************************"
        cat source/Commands/index.rst
        exit 1
      fi
  only:
    refs:
      - branches
    variables:
      - $SWEEP != "true"

# Integration tests
integration_tests:
  stage: test
  image: registry.cern.ch/docker.io/library/alpine:3.19.1
  tags:
    - docker-privileged-xl
  services:
    - docker:25.0.1-dind
  variables:
    DOCKER_TLS_CERTDIR: ""
    DOCKER_HOST: tcp://docker:2375/
  rules:
    # Using rules causes "Pipelines for Merge Requests" to be triggered, so disable them explicitly
    # https://docs.gitlab.com/ee/ci/merge_request_pipelines/
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD == null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: true
    - if: '$LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD != null && $CI_MERGE_REQUEST_ID == null && $SWEEP != "true"'
      allow_failure: false
  parallel:
    matrix:
      - TEST_COMMAND:
          - python3 ./integration_tests.py create "LHCBDIRAC_RELEASE=$${lhcbdiracTag}" TEST_HTTPS=No --extra-module LHCbDIRAC=../LHCbDIRAC
  before_script:
    # Install dependencies
    - apk add docker py3-pip python3 bash git docker-compose jq curl
    - python3 -m venv .venv
    - source .venv/bin/activate
    - python3 -m pip install typer pyyaml gitpython diraccfg packaging
    # Find the latest LHCbDIRAC release
    - git remote add upstream https://gitlab.cern.ch/lhcb-dirac/LHCbDIRAC.git && git fetch upstream
    - lhcbdiracTag=$(git describe --tags --match '*[0-9]*' --exclude 'v[0-9]r*' --exclude 'v[0-9][0-9]r*' --abbrev=0)
    - echo "lhcbdiracTag=${lhcbdiracTag}"
    # Find the latest compatible DIRAC release
    - diracBranch=$(grep 'DIRAC ~=' setup.cfg | sed -E 's@.+~= *([0-9])+\.([0-9]+)\.[0-9]+(a[0-9]+)?@rel-v\1r\2@g')
    - echo "Found DIRAC branch $diracBranch"
    # Query GitHub to find if the rel-vXrY branch exists, fall back to integration if it does not
    - |
      if ! curl --fail --no-progress-meter --head -L "https://codeload.github.com/DIRACGrid/DIRAC/zip/refs/heads/${diracBranch}" > /dev/null; then
        diracBranch="integration"
      fi
    - echo "diracBranch=${diracBranch}"
    # Checkout the latest compatible DIRAC release
    - git clone https://github.com/DIRACGrid/DIRAC.git -b "${diracBranch}" ../DIRAC
    - cd ../DIRAC
    # Login to the GitLab container registry for access to Oracle
    - |
      if [[ -z "${LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD:-}" ]]; then
        echo -e '\033[1m\033[31mError LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD variable is not defined!!!\033[0m';
        echo -e 'Due to oracle licensing restrictions CI jobs need a secret to be able able to access the bookkeeping database container.';
        echo -e 'For this test to run you will have to ask another LHCbDIRAC developer to give you access.';
        echo -e "You can then manually copy to a masked variable to your fork's configuration using the instructions at:";
        echo -e ' > https://docs.gitlab.com/ee/ci/variables/#via-the-ui';
        exit 67;
      fi
    - echo "${LHCB_BOOKKEEPING_DB_IMAGE_PASSWORD}" | docker login --username gitlab+deploy-token-794 --password-stdin https://gitlab-registry.cern.ch
  script:
    - echo $TEST_COMMAND
    - eval $TEST_COMMAND

# ### deploy_release stage ###

# When we have a git tag, create a docker image used by kubernetes and upload it to the gitlab registry
build_docker_image:
  stage: deploy_release
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - |
      /kaniko/executor \
        --context $CI_PROJECT_DIR/container/lhcbdirac \
        --dockerfile $CI_PROJECT_DIR/container/lhcbdirac/Dockerfile \
        --destination $CI_REGISTRY_IMAGE:${CI_COMMIT_TAG} \
        --build-arg LHCB_DIRAC_VERSION=${CI_COMMIT_TAG}


test_docker_image:
  only:
    variables:
      - $SWEEP != "true"
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - |
      /kaniko/executor \
        --context $CI_PROJECT_DIR/container/lhcbdirac \
        --dockerfile $CI_PROJECT_DIR/container/lhcbdirac/Dockerfile \
        --build-arg LHCB_DIRAC_VERSION=v11.0.43 \
        --no-push

# Image for propagation of gitlab MRs from master to devel
build_sweeper_image:
  stage: images
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  image:
    name: gcr.io/kaniko-project/executor:v1.14.0-debug
    entrypoint: [""]
  script:
    - echo "{\"auths\":{\"${CI_REGISTRY}\":{\"username\":\"${CI_REGISTRY_USER}\",\"password\":\"${CI_REGISTRY_PASSWORD}\"}}}" > /kaniko/.docker/config.json
    - |
      /kaniko/executor \
        --context $CI_PROJECT_DIR/container/sweeper \
        --dockerfile $CI_PROJECT_DIR/container/sweeper/Dockerfile \
        --destination $CI_REGISTRY_IMAGE:sweeper

# Template job used by deploy_on_cvmfs and set_cvmfs_prod_link
.cvmfs_task:
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  before_script:
    - |
      pat="([0-9\.]+)((a|b|rc)([0-9]+))"
      if [[ "${CI_COMMIT_TAG}" =~ $pat ]]; then
        LHCB_DIRAC_SETUP="LHCb-Certification"
      else
        LHCB_DIRAC_SETUP="LHCb-Production"
      fi


# Deploy the release on CVMFS but don't set the prod symlink
deploy_on_cvmfs:
  extends: .cvmfs_task
  stage: deploy_release
  script:
    - ./.ci/trigger_cvmfs_deployment.sh deploy "${LHCB_DIRAC_SETUP}" "${CI_COMMIT_TAG}"


# ### update instance stage ###
# Template job used to get lhcb_admin proxy for production or certification
.getAdminProxy:
  tags:
    - cvmfs
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  image: registry.cern.ch/docker.io/library/almalinux:9
  before_script:
    - mkdir -p ~/.globus
    - echo "$BOT_USERCERT_PEM" | tr -d '\r' > ~/.globus/usercert.pem
    - echo "$BOT_USERKEY_PEM" | tr -d '\r' > ~/.globus/userkey.pem
    - chmod 400 ~/.globus/userkey.pem
    - chmod 644  ~/.globus/usercert.pem
    - |
      pat="([0-9\.]+)((a|b|rc)([0-9]+))"
      if [[ "${CI_COMMIT_TAG}" =~ $pat ]]; then
        INSTANCE="lhcbdev.cern.ch"
      else
        INSTANCE="lhcb.cern.ch"
      fi
    - echo "Using LHCbDIRAC installation from /cvmfs/${INSTANCE}/lhcbdirac/"
    - source /cvmfs/${INSTANCE}/lhcbdirac/lhcbdirac
    - echo $BOT_CERT_PASSPHRASE | lhcb-proxy-init -g lhcb_admin -p

update_instance:
  extends: .getAdminProxy
  when: manual
  stage: update
  script:
    - |
      if [ ! -L "/cvmfs/${INSTANCE}/lhcbdirac/${CI_COMMIT_TAG}-x86_64" ]; then
       echo "New version not found in CVMFS"
       exit 1
      fi
    - |
      if [[ "${INSTANCE}" == "lhcb.cern.ch" ]]; then
        dirac-admin-sysadmin-cli <<< "show hosts" | awk '{print $2}' | grep -E "(\..*){2}" > allHosts.txt
        grep ".cern.ch" allHosts.txt > primaryHosts.txt
        grep -v ".cern.ch" allHosts.txt > secondaryHosts.txt
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2 --hosts=secondaryHosts.txt || echo "Allow failure"
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2 --hosts=primaryHosts.txt --excludeHosts=datamover01.cern.ch
      else
        dirac-admin-update-instance ${CI_COMMIT_TAG} --retry=2
      fi
    - dirac-admin-update-pilot ${CI_COMMIT_TAG}


# ### finish_up stage ###

# If we are triggering on a pre release,
# tag the docker image we just built as 'devel'
# check https://gitlab.cern.ch/ci-tools/docker-image-tools for the image doc
add_devel_tag:
  image: gitlab-registry.cern.ch/ci-tools/docker-image-tools:add-tag
  stage: finish_up
  script:
    - echo "Adding tag.."
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
      - ($CI_COMMIT_TAG =~ /.*(a|b|rc)[0-9]+/ && $SWEEP != "true")
  variables:
    DOCKER_IMAGE_PATH: $CI_REGISTRY_IMAGE
    OLDTAG: ${CI_COMMIT_TAG}
    NEWTAG: devel
    GIT_STRATEGY: none

# Use a manual job to change the version of the prod symlink
# This task can be ran multiple times which means an old pipeline can be restarted to roll back the prod symlink
set_cvmfs_prod_link:
  extends: .cvmfs_task
  stage: finish_up
  when: manual
  script:
    - ./.ci/trigger_cvmfs_deployment.sh set-prod "${LHCB_DIRAC_SETUP}" "${CI_COMMIT_TAG}"


# Update the Helm configuration
# by setting LHCB_DIRAC_VERSION
# and triggering lhcb-dirac/diracchart pipeline
deploy_k8:
  stage: finish_up
  allow_failure: true
  variables:
    LHCB_DIRAC_VERSION: ${CI_COMMIT_TAG}
  only:
    refs:
      - tags@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - /^v[0-9]+\.[0-9]+\.[0-9]+$/
  trigger:
    project: lhcb-dirac/diracchart
    strategy: depend

# Job for starting the tag procedure
make_tag (set VERSION to override):
  image: registry.cern.ch/docker.io/library/python:3.11
  stage: tag
  when: manual
  variables:
    GIT_STRATEGY: clone
    VERSION: ''
  # Can be run only on master and devel branch of the main repo
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
      - devel@lhcb-dirac/LHCbDIRAC
    variables:
      - $SWEEP != "true"
  except:
    - tags@lhcb-dirac/LHCbDIRAC
  before_script:
    - apt update && apt install -y jq git curl
    - pip install -U requests python-dateutil pytz
    # init the dirac bot gitlab user
    - eval `ssh-agent -s`
    - mkdir -p ~/.ssh && chmod 700 ~/.ssh && echo "$SSH_KEY" | tr -d '\r' > /root/.ssh/id_rsa && chmod 700 /root/.ssh/id_rsa
    - ssh-keyscan -p 7999 gitlab.cern.ch >> /root/.ssh/known_hosts
    - git config --global user.email "dirac.bot@cern.ch" && git config --global user.name "LHCbDIRAC Bot" && git config --global pull.rebase true
    - git clone ssh://git@gitlab.cern.ch:7999/lhcb-dirac/LHCbDIRAC.git LHCbDIRAC
    - cd LHCbDIRAC
    - git checkout ${CI_COMMIT_REF_NAME}
    - echo "GITLABTOKEN = \"$GITLABTOKEN\"" >& GitTokens.py
    # Get the tagging tool from the DIRACGrid repo
    - curl -O https://raw.githubusercontent.com/DIRACGrid/DIRAC/integration/docs/diracdoctools/scripts/dirac-docs-get-release-notes.py
  script:
    - set -eu
    # Aggregate the release notes for the current branch
    - python dirac-docs-get-release-notes.py --sinceLatestTag -g  -i 3588 --branches ${CI_COMMIT_REF_NAME} | sed '1,2d' | tee notes.txt
    # Compute VERSION if it's not already set
    - CURRENT_VERSION=$(git describe --tags --match '*[0-9]*' --exclude 'v[0-9]r*' --exclude 'v[0-9][0-9]r*' --abbrev=0)
    # If the version isn't manually overridden with the "VERSION" variable, bump the smallest component
    #   v10.1.0    becomes v10.1.1
    #   v10.1.12   becomes v10.1.13
    #   v10.1.0a5  becomes v10.1.0a6
    #   v10.1.0rc1 becomes v10.1.0rc2
    - |
      if [ -z ${VERSION:-} ]; then
        majorpart="${CURRENT_VERSION%.*}."
        minorpart="${CURRENT_VERSION##*.}"
        pat="([0-9]+)((a|b|rc)([0-9]+))"
        if [[ "$minorpart" =~ $pat ]]; then
          majorpart=${majorpart}$(echo "$minorpart" | sed -E "s@$pat@\1\3@g")
          minorpart=$(echo "$minorpart" | sed -E "s@$pat@\4@g")
        fi
        VERSION=$majorpart$((10#$minorpart+1))
      fi
    - echo Current version is $CURRENT_VERSION will bump to $VERSION
    - SERIES=$(echo $VERSION | sed -E 's@^v([0-9]+)\.([0-9]+).+@v\1r\2@g')
    - echo Release series is $SERIES
    # insert new release notes into the changelog
    - echo "[$VERSION]" > newNotes.txt
    # Ensure CHANGELOG/$SERIES exists
    - touch CHANGELOG/$SERIES
    - cat notes.txt CHANGELOG/$SERIES >> newNotes.txt && mv newNotes.txt CHANGELOG/$SERIES
    # show all the changes we have done
    - git diff
    # commit all the changes to the current branch
    - git add CHANGELOG/$SERIES
    - git commit -m "Update versions and changelog for release of ${VERSION}"
    # tag current branch and push it
    - git tag -a ${VERSION} -m ${VERSION}
    - git push --tags origin ${CI_COMMIT_REF_NAME}
    # If we just tagged master, update also update the CHANGELOG in devel
    - |
      if [[ "${CI_COMMIT_REF_NAME}" == "master" ]]; then
        git fetch origin && git checkout devel && git reset --hard origin/devel
        git checkout origin/master -- "CHANGELOG/$SERIES"
        git add "CHANGELOG/$SERIES"
        git commit -m "Add release ${VERSION} to releases.cfg and CHANGELOG"
        git push origin devel
      fi
    # Create release in gitlab
    - python dirac-docs-get-release-notes.py -g -i 3588 --tagName ${VERSION} --releaseNotes notes.txt --deployRelease

MR_SWEEP:
  stage: sweep
  image: gitlab-registry.cern.ch/lhcb-dirac/lhcbdirac:sweeper
  needs: []
  variables:
    SWEEP_SINCE: "1 month ago"
    SWEEP_UNTIL: "now"
  only:
    refs:
      - master@lhcb-dirac/LHCbDIRAC
  before_script: []
  after_script: []
  script:
    - python /sweep_MR.py -p ${CI_PROJECT_NAMESPACE}/${CI_PROJECT_NAME} -b origin/${CI_COMMIT_REF_NAME} -t ${SWEEP_TOKEN} -s "${SWEEP_SINCE}" -u "${SWEEP_UNTIL}" --repository-root ${PWD}

build-wheel-and-maybe-deploy-to-pypi:
  stage: upload_pypi
  image: registry.cern.ch/docker.io/library/python:3.11
  before_script:
    - pip install build twine wheel setuptools-scm[toml]
  script:
    - python -m build
    - twine check --strict dist/*
    # If this is a tag upload the distributions to PyPI
    - |
      if [ -n "${CI_COMMIT_TAG:-}" ]; then
        if [ -z "$TWINE_PASSWORD" ] ; then echo "Set TWINE_PASSWORD in CI variables"; exit 1; fi
        twine upload -u __token__ dist/*
        echo "Sleeping for 60 seconds to ensure PyPI's CDN has time to update"
        sleep 60
      fi

run-dms-tests:
  image: registry.cern.ch/docker.io/library/almalinux:9
  timeout: 3h
  when: manual
  variables:
    # The DIRAC repo to use for getting the test code'
    DIRAC_test_repo: "DIRACGrid"
    # The DIRAC branch to use for getting the test code
    DIRAC_test_branch: integration
    # The LHCbDIRAC repo to use for getting the test code
    LHCbDIRAC_test_repo: 'lhcb-dirac'
    # The LHCbDIRAC branch to use for getting the test code
    LHCbDIRAC_test_branch: devel
    # 'CS URL (put HTTPs if necessary
    CSURL: 'https://lhcb-cert-conf-dirac.cern.ch:9135/Configuration/Server'
    DOCKER_TLS_CERTDIR: "/certs"
    # SE_config should be defined as a gitlab variable
  script:
    - bash tests/System/StorageTests/run-storageelement-tests.sh
